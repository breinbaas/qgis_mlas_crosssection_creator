from qgis.core import (
    QgsApplication, QgsTask, QgsMessageLog, Qgis
)

MESSAGE_CATEGORY = 'CrosssectionCreateTask'

class CrosssectionCreateTask(QgsTask):
    def __init__(self, description, crosssectioncreator):
        super().__init__(description, QgsTask.CanCancel)
        self.crosssectioncreator = crosssectioncreator
        self.exception = None
    
    def run(self):                
        QgsMessageLog.logMessage(
            f'Started crosssection creator "{self.description()}"',
            MESSAGE_CATEGORY, 
            Qgis.Info
        )

        try:
            self.crosssectioncreator.execute()
        except Exception as e:
            self.exception = e
            return False
        
        return True

    def finished(self, result):
        if result:
            QgsMessageLog.logMessage(
                'CrosssectionCreateTask completed', 
                MESSAGE_CATEGORY, 
                Qgis.Success
            )
        else:
            if self.exception is None:
                QgsMessageLog.logMessage(
                    'CrosssectionCreateTask not successful but without exception (probably the task was manually canceled by the user)',
                    MESSAGE_CATEGORY, 
                    Qgis.Warning
                )
            else:
                QgsMessageLog.logMessage(
                    f'CrosssectionCreateTask got Exception: {self.exception}',
                    MESSAGE_CATEGORY, Qgis.Critical)
                raise self.exception



    def cancel(self):
        QgsMessageLog.logMessage(
            'CrosssectionCreateTask was canceled',
            MESSAGE_CATEGORY, 
            Qgis.Info
        )
        super().cancel()